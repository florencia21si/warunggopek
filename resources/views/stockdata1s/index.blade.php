@extends('default')

@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stock Data</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
</head>
<body>
<div class="container mt-5">
        <div class="row">   
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                    <div class="card-body"> <center>
                        <a href="{{ route('stockdata1s.create')}}" class="btn btn-md btn-success mb-3">
                            TAMBAH DATA STOCK
                        </a> </center>
	<div class="d-flex justify-content-end mb-3"><a href="{{ route('stockdata1s.create') }}" class="btn btn-info">Create</a></div>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th>id</th>
				<th>Nama Bahan</th>
				<th>Jumlah</th>
				<th>Update Terakhir</th>
				<th>Status</th>

				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($stockdata1s as $stockdata1)

				<tr>
					<td>{{ $stockdata1->id }}</td>
					<td>{{ $stockdata1->bahan }}</td>
					<td>{{ $stockdata1->jumlah }}</td>
					<td>{{ $stockdata1->updateterakhir }}</td>
					<td>{{ $stockdata1->status }}</td>

					<td>
						<div class="d-flex gap-2">
                            <a href="{{ route('stockdata1s.show', [$stockdata1->id]) }}" class="btn btn-info">Show</a>
                            <a href="{{ route('stockdata1s.edit', [$stockdata1->id]) }}" class="btn btn-primary">Edit</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['stockdata1s.destroy', $stockdata1->id]]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </div>
					</td>
				</tr>

			@endforeach
		</tbody>
	</table>
					</div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>        
        //message with toastr
        @if (session()->has('success'))
            toastr.success('{{ session('success') }}', 'BERHASIL!');
        @elseif (session()->has('error'))
            toastr.error('{{ session('error') }}', 'GAGAL!');
        @endif
    </script>
</body>
</html>
@stop
