@extends('default')

@section('content')
<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tambah Data</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card border-0 shadow rounded">
                        <div class="card-body">
                            <form action="{{ route('stockdata1s.store')}}" method="post" enctype="multipart/form-data">
	@if($errors->any())
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
				{{ $error }} <br>
			@endforeach
		</div>
	@endif

	{!! Form::open(['route' => 'stockdata1s.store']) !!}

		<div class="mb-3">
			{{ Form::label('bahan', 'Bahan', ['class'=>'form-label']) }}
			{{ Form::text('bahan', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('jumlah', 'Jumlah', ['class'=>'form-label']) }}
			{{ Form::text('jumlah', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('updateterakhir', 'Update Terakhir', ['class'=>'form-label']) }}
			{{ Form::text('updateterakhir', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('status', 'Status', ['class'=>'form-label']) }}
			{{ Form::text('status', null, array('class' => 'form-control')) }}
		</div>


		{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}

	{{ Form::close() }}
						</div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <script>        
            CKEDITOR.replace('description')
        </script>
    </body>
    </html>

@stop