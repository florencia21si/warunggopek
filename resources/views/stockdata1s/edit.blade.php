@extends('default')

@section('content')

	@if($errors->any())
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
				{{ $error }} <br>
			@endforeach
		</div>
	@endif

	{{ Form::model($stockdata1, array('route' => array('stockdata1s.update', $stockdata1->id), 'method' => 'PUT')) }}

		<div class="mb-3">
			{{ Form::label('bahan', 'Bahan', ['class'=>'form-label']) }}
			{{ Form::text('bahan', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('jumlah', 'Jumlah', ['class'=>'form-label']) }}
			{{ Form::text('jumlah', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('updateterakhir', 'Updateterakhir', ['class'=>'form-label']) }}
			{{ Form::text('updateterakhir', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('status', 'Status', ['class'=>'form-label']) }}
			{{ Form::text('status', null, array('class' => 'form-control')) }}
		</div>

		{{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

	{{ Form::close() }}
@stop
