<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Stockdatum;
use App\Http\Requests\StockdatumRequest;

class StockdatasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $stockdatas= Stockdatum::all();
        return view('stockdatas.index', ['stockdatas'=>$stockdatas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('stockdatas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StockdatumRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StockdatumRequest $request)
    {
        $stockdatum = new Stockdatum;
		$stockdatum->bahan = $request->input('bahan');
		$stockdatum->jumlah = $request->input('jumlah');
		$stockdatum->updateterakhir = $request->input('updateterakhir');
		$stockdatum->status = $request->input('status');
        $stockdatum->save();

        return to_route('stockdatas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $stockdatum = Stockdatum::findOrFail($id);
        return view('stockdatas.show',['stockdatum'=>$stockdatum]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $stockdatum = Stockdatum::findOrFail($id);
        return view('stockdatas.edit',['stockdatum'=>$stockdatum]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StockdatumRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StockdatumRequest $request, $id)
    {
        $stockdatum = Stockdatum::findOrFail($id);
		$stockdatum->bahan = $request->input('bahan');
		$stockdatum->jumlah = $request->input('jumlah');
		$stockdatum->updateterakhir = $request->input('updateterakhir');
		$stockdatum->status = $request->input('status');
        $stockdatum->save();

        return to_route('stockdatas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $stockdatum = Stockdatum::findOrFail($id);
        $stockdatum->delete();

        return to_route('stockdatas.index');
    }
}
