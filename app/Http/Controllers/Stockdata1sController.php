<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Stockdata1;
use App\Http\Requests\Stockdata1Request;

class Stockdata1sController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $stockdata1s= Stockdata1::all();
        return view('stockdata1s.index', ['stockdata1s'=>$stockdata1s]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('stockdata1s.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Stockdata1Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Stockdata1Request $request)
    {
        $stockdata1 = new Stockdata1;
		$stockdata1->bahan = $request->input('bahan');
		$stockdata1->jumlah = $request->input('jumlah');
		$stockdata1->updateterakhir = $request->input('updateterakhir');
		$stockdata1->status = $request->input('status');
        $stockdata1->save();

        return to_route('stockdata1s.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $stockdata1 = Stockdata1::findOrFail($id);
        return view('stockdata1s.show',['stockdata1'=>$stockdata1]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $stockdata1 = Stockdata1::findOrFail($id);
        return view('stockdata1s.edit',['stockdata1'=>$stockdata1]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Stockdata1Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Stockdata1Request $request, $id)
    {
        $stockdata1 = Stockdata1::findOrFail($id);
		$stockdata1->bahan = $request->input('bahan');
		$stockdata1->jumlah = $request->input('jumlah');
		$stockdata1->updateterakhir = $request->input('updateterakhir');
		$stockdata1->status = $request->input('status');
        $stockdata1->save();

        return to_route('stockdata1s.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $stockdata1 = Stockdata1::findOrFail($id);
        $stockdata1->delete();

        return to_route('stockdata1s.index');
    }
}
